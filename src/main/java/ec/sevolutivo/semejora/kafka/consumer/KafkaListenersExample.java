package ec.sevolutivo.semejora.kafka.consumer;

import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;

@Component
class KafkaListenersExample {

    Logger LOG = LoggerFactory.getLogger(KafkaListenersExample.class);




    @KafkaListener(topics = "${application.topic}")
    void listener(String data) {
        LOG.info(data);
    }

}
